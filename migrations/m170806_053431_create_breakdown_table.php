<?php

use yii\db\Migration;

/**
 * Handles the creation of table `breakdown`.
 */
class m170806_053431_create_breakdown_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('breakdown', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'level' => $this->integer(),
            'status' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('breakdown');
    }
}
