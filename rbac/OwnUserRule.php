<?php
namespace app\rbac;

use yii\rbac\Rule;
use app\models\User;
use yii\web\NotFoundHttpException;
use Yii; 

class OwnUserRule extends Rule
{

	public $name = 'OwnUserRule';

	public function execute($user, $item, $params)
		{	
			if(isset($_GET['id']) && isset($user)){
				if($_GET['id'] == $user)
					return true;
			}
		
			return false;
		}
		
}