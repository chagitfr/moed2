<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\Level;


/* @var $this yii\web\View */
/* @var $model app\models\Breakdown */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="breakdown-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'status')-> dropDownList(Status::getStatuses()) ?>
	
	
	<?php if(\Yii::$app->user->can('viewUrgentBreakdown')){ ?>
			<div style="display:none;"> <?= $form->field($model, 'level')->textInput(['value'=>3]) ?> </div>
		<?php }else{ ?>
			<?= $form->field($model, 'level')-> dropDownList(Level::getLeveles()) ?>
	<?php } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
