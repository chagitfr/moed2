<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\models\User;
use app\models\Level;
use app\models\Status;

/**
 * This is the model class for table "breakdown".
 *
 * @property integer $id
 * @property string $title
 * @property integer $level
 * @property integer $status
 */
class Breakdown extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'breakdown';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level_name', 'status_name'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'level' => 'Level',
            'status' => 'Status',
        ];
		
    }
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }	
	public function getLevelItem()
    {
        return $this->hasOne(Level::className(), ['id' => 'level']);
    }
	
}
