<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class AddruleController extends Controller
{
	//משכפלים במקרה של רולים נוספים
	public function actionOwnuser()
	{ 
		$auth = Yii::$app->authManager; 
		$rule = new \app\rbac\OwnUserRule; //לשים לב לשם התיקיה ב rbac
		$auth->add($rule);
	}
	
	public function actionUrgentbreakdown()
	{ 
		$auth = Yii::$app->authManager; 
		$rule = new \app\rbac\UrgentBreakdownRule; //לשים לב לשם התיקיה ב rbac
		$auth->add($rule);
	}
}